#/usr/bin/env bash
docker run --name teadb_postgres \
            -p 5432:5432 \
            -e POSTGRES_USER=teadb \
            -e POSTGRES_PASSWORD=teadb \
            -e POSTGRES_DB=teadb_development \
            -d postgres:14.1-alpine
