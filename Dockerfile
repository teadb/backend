FROM haskell:9.2.7
WORKDIR /opt/teadb
RUN stack update && apt update && apt install -y libpq-dev
COPY . /opt/teadb
RUN stack build --only-dependencies
RUN stack build
CMD ["stack", "exec", "teadb"]
