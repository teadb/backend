-- Migration: teas
-- Created at: 2020-10-02 15:24:03
-- ====  UP  ====

BEGIN;
  CREATE TABLE teas (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT,
    country TEXT,
    description TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
  );

  ALTER TABLE teas
  ADD CONSTRAINT name_not_null
  CHECK (
    name IS NOT NULL
  );

  ALTER TABLE teas
  ADD CONSTRAINT name_is_unique
  UNIQUE (name);

COMMIT;

-- ==== DOWN ====

BEGIN;
  DROP TABLE teas;

COMMIT;
