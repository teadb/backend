-- Migration: steepings
-- Created at: 2020-10-09 21:20:08
-- ====  UP  ====

BEGIN;
  CREATE TABLE steepings (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    rating SMALLINT,
    temperature SMALLINT,
    steeping_time SMALLINT,
    description TEXT,
    tea_id BIGINT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
  );

  ALTER TABLE steepings
  ADD CONSTRAINT tea_id_references_teas
  FOREIGN KEY(tea_id)
  REFERENCES teas(id)
  ON DELETE CASCADE;

COMMIT;

-- ==== DOWN ====

BEGIN;
  DROP TABLE steepings;

COMMIT;
