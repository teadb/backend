-- Migration: add-water-amount-to-steepings
-- Created at: 2023-05-26 12:16:08
-- ====  UP  ====

BEGIN;
  ALTER TABLE steepings
  ADD COLUMN water_amount SMALLINT NOT NULL DEFAULT 0;

COMMIT;

-- ==== DOWN ====

BEGIN;
  ALTER TABLE steepings
  DROP COLUMN amount;

COMMIT;
