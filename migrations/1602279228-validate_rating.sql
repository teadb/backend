-- Migration: validate_rating
-- Created at: 2020-10-09 21:33:48
-- ====  UP  ====

BEGIN;
  ALTER TABLE steepings
  ADD CONSTRAINT rating_is_greater_than_zero
  CHECK (
    rating > 0
  );

COMMIT;

-- ==== DOWN ====

BEGIN;
  ALTER TABLE steepings
  DROP CONSTRAINT rating_is_greater_than_zero;

COMMIT;
