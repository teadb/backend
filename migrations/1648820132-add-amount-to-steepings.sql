-- Migration: add-amount-to-steepings
-- Created at: 2022-04-01 13:35:32
-- ====  UP  ====

BEGIN;
  ALTER TABLE steepings
  ADD COLUMN amount SMALLINT NOT NULL DEFAULT 0;

COMMIT;

-- ==== DOWN ====

BEGIN;
  ALTER TABLE steepings
  DROP COLUMN amount;

COMMIT;
