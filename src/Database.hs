module Database where

import qualified Data.Aeson (ToJSON)
import Data.Int ()
import Database.PostgreSQL.Simple
import Statements
import Steeping
import Tea

getAllTeas :: ConnectInfo -> IO [Tea]
getAllTeas connectionSettings = do
  connection <- connect connectionSettings
  query_ connection Statements.selectAllTeas

getAllSteepings :: ConnectInfo -> IO [Steeping]
getAllSteepings connectionSettings = do
  connection <- connect connectionSettings
  query_ connection Statements.selectAllSteepings

getTeaById :: ConnectInfo -> Int64 -> IO [Tea]
getTeaById connectionSettings teaId = do
  connection <- connect connectionSettings
  query connection Statements.selectTeaById (Only teaId)

createNewTea :: ConnectInfo -> Text -> Text -> Text -> IO [Tea]
createNewTea connectionSettings name country description = do
  connection <- connect connectionSettings
  Database.PostgreSQL.Simple.returning connection Statements.insertTea [(name, country, description)]

destroyTeaById :: ConnectInfo -> Int64 -> IO Int64
destroyTeaById connectionSettings teaId = do
  connection <- connect connectionSettings
  Database.PostgreSQL.Simple.execute connection Statements.deleteTea (Only teaId)

getSteepingsForTea :: ConnectInfo -> Int -> IO [Steeping]
getSteepingsForTea connectionSettings teaId = do
  connection <- connect connectionSettings
  query connection Statements.steepingsForTea (Only teaId)

createNewSteeping :: ConnectInfo -> Int -> Int -> Int -> Text -> Int64 -> Int -> Int -> IO [Steeping]
createNewSteeping connectionSettings rating temperature steeping_time description tea_id amount water_amount = do
  connection <- connect connectionSettings
  Database.PostgreSQL.Simple.returning connection Statements.insertSteeping [(rating, temperature, steeping_time, description, tea_id, amount, water_amount)]

destroySteeping :: ConnectInfo -> Int -> Int -> IO Int64
destroySteeping connectionSettings teaId steepingId = do
  connection <- connect connectionSettings
  Database.PostgreSQL.Simple.execute connection Statements.deleteSteeping (steepingId, teaId)
