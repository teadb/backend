module Statements where

import Database.PostgreSQL.Simple

selectAllTeas :: Query
selectAllTeas = "SELECT * FROM teas"

selectAllSteepings :: Query
selectAllSteepings = "SELECT * FROM steepings"

selectTeaById :: Query
selectTeaById = "SELECT * FROM teas WHERE id = ?"

insertTea :: Query
insertTea = "INSERT INTO teas(name, country, description) VALUES (?, ?, ?) RETURNING *"

deleteTea :: Query
deleteTea = "DELETE FROM teas WHERE id = ?"

steepingsForTea :: Query
steepingsForTea = "SELECT * FROM steepings WHERE tea_id = ?"

insertSteeping :: Query
insertSteeping = "INSERT INTO steepings(rating, temperature, steeping_time, description, tea_id, amount, water_amount) VALUES (?,?,?,?,?,?,?) RETURNING *"

deleteSteeping :: Query
deleteSteeping = "DELETE FROM steepings WHERE id = ? AND tea_id = ?"
