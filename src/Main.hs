{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad.IO.Class ()
import qualified Data.Aeson (ToJSON)
import Database
import Database.PostgreSQL.Simple
import Healthcheck
import Network.HTTP.Types
import Network.Wai (Middleware)
import Network.Wai.Middleware.Cors
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Steeping
import System.Environment.MrEnv (envAsBool, envAsInt, envAsInteger, envAsString)
import System.IO (BufferMode (LineBuffering), hSetBuffering, stdout)
import Tea
import Web.Scotty

instance Data.Aeson.ToJSON ConnectInfo

allowCors :: Middleware
allowCors = cors (const $ Just appCorsResourcePolicy)

appCorsResourcePolicy :: CorsResourcePolicy
appCorsResourcePolicy =
  simpleCorsResourcePolicy
    { corsMethods = simpleMethods <> ["OPTIONS", "DELETE"],
      corsRequestHeaders = simpleHeaders
    }

main = do
  hSetBuffering stdout LineBuffering

  putStrLn "Starting TeaDB web server..."

  port <- envAsInt "TEADB_PORT" 6666
  dbHost <- envAsString "TEADB_DB_HOST" "localhost"
  dbPort <- envAsInt "TEADB_DB_PORT" 5432
  dbName <- envAsString "TEADB_DB_NAME" "teadb_development"
  dbUser <- envAsString "TEADB_DB_USER" "teadb"
  dbPassword <- envAsString "TEADB_DB_PASSWORD" "teadb"

  let connectionSettings =
        defaultConnectInfo
          { connectHost = dbHost,
            connectPort = fromIntegral dbPort,
            connectDatabase = dbName,
            connectUser = dbUser,
            connectPassword = dbPassword
          }

  scotty port $ do
    Web.Scotty.middleware allowCors
    Web.Scotty.middleware logStdoutDev
    Web.Scotty.get "/healthcheck" $ do
      status ok200
      json connectionSettings
      json Healthcheck.allSystemsGo
    Web.Scotty.get "/" $ do
      status ok200
    Web.Scotty.get "/teas" $ do
      allTeas <- liftIO $ Database.getAllTeas connectionSettings
      status ok200
      json allTeas
    Web.Scotty.get "/steepings" $ do
      allSteepings <- liftIO $ Database.getAllSteepings connectionSettings
      status ok200
      json allSteepings
    Web.Scotty.post "/teas" $ do
      parsedTea <- jsonData :: ActionM NewTea
      createdTea <-
        liftIO
          ( Database.createNewTea
              connectionSettings
              (name (parsedTea :: NewTea))
              (country (parsedTea :: NewTea))
              (description (parsedTea :: NewTea))
          )
      status created201
      json createdTea
    Web.Scotty.get "/teas/:id" $ do
      id <- param "id"
      tea <- liftIO (head <$> Database.getTeaById connectionSettings id)
      status ok200
      json tea
    Web.Scotty.delete "/teas/:tea_id" $ do
      teaId <- param "tea_id"
      rowsRemoved <- liftIO (Database.destroyTeaById connectionSettings teaId)
      status ok200
    Web.Scotty.get "/teas/:id/steepings" $ do
      id <- param "id"
      steepings <- liftIO (Database.getSteepingsForTea connectionSettings id)
      status ok200
      json steepings
    Web.Scotty.delete "/teas/:tea_id/steepings/:steeping_id" $ do
      teaId <- param "tea_id"
      steepingId <- param "steeping_id"
      rowsRemoved <- liftIO (Database.destroySteeping connectionSettings teaId steepingId)
      status ok200
    Web.Scotty.post "/steepings" $ do
      parsedSteeping <- jsonData :: ActionM NewSteeping
      createdSteeping <-
        liftIO
          ( Database.createNewSteeping
              connectionSettings
              (rating (parsedSteeping :: NewSteeping))
              (temperature (parsedSteeping :: NewSteeping))
              (steeping_time (parsedSteeping :: NewSteeping))
              (description (parsedSteeping :: NewSteeping))
              (tea_id (parsedSteeping :: NewSteeping))
              (amount (parsedSteeping :: NewSteeping))
              (water_amount (parsedSteeping :: NewSteeping))
          )
      status created201
      json createdSteeping
