module Tea where

import qualified Data.Aeson (FromJSON, ToJSON)
import Data.Int ()
import Data.Text ()
import Data.Time
import Database.PostgreSQL.Simple.FromRow
import GHC.Generics ()
import GHC.Show ()

instance FromRow Tea where
  fromRow =
    Tea
      <$> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field

data Tea = Tea
  { id :: Int64,
    name :: Text,
    country :: Text,
    description :: Text,
    created_at :: Data.Time.ZonedTime,
    updated_at :: Data.Time.ZonedTime
  }
  deriving (Show, Generic)

instance Data.Aeson.ToJSON Tea

instance Data.Aeson.FromJSON Tea

data NewTea = NewTea
  { name :: Text,
    country :: Text,
    description :: Text
  }
  deriving (Show, Generic)

instance Data.Aeson.ToJSON NewTea

instance Data.Aeson.FromJSON NewTea
