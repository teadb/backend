module Steeping where

import qualified Data.Aeson (FromJSON, ToJSON)
import Data.Int ()
import Data.Text ()
import Data.Time
import Database.PostgreSQL.Simple.FromRow
import GHC.Generics ()
import GHC.Show ()

data Steeping = Steeping
  { id :: Int64,
    rating :: Int,
    temperature :: Int,
    steeping_time :: Int,
    description :: Text,
    tea_id :: Int64,
    created_at :: Data.Time.ZonedTime,
    updated_at :: Data.Time.ZonedTime,
    amount :: Int,
    water_amount :: Int
  }
  deriving (Show, Generic)

instance FromRow Steeping where
  fromRow =
    Steeping
      <$> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field
      <*> field

instance Data.Aeson.ToJSON Steeping

instance Data.Aeson.FromJSON Steeping

data NewSteeping = NewSteeping
  { rating :: Int,
    temperature :: Int,
    steeping_time :: Int,
    description :: Text,
    tea_id :: Int64,
    amount :: Int,
    water_amount :: Int
  }
  deriving (Show, Generic)

instance Data.Aeson.ToJSON NewSteeping

instance Data.Aeson.FromJSON NewSteeping
