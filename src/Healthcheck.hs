{-# LANGUAGE DeriveGeneric,OverloadedStrings #-}

module Healthcheck(allSystemsGo) where

import qualified Data.Aeson (ToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import GHC.Show ()

data ServiceState = Up | Degraded | Down deriving (Show, Generic)

instance Data.Aeson.ToJSON ServiceState

data HealthcheckResponse = HealthcheckResponse
  { name :: Text,
    state :: ServiceState
  }
  deriving (Show, Generic)

instance Data.Aeson.ToJSON HealthcheckResponse

allSystemsGo :: HealthcheckResponse
allSystemsGo =
  HealthcheckResponse
    { name = "web server",
      state = Up
    }
